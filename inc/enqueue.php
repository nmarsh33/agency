<?php
/**
 *
 * Styles: Frontend with no conditions, Add Custom styles to wp_head
 *
 * @since  1.0
 *
 */
add_action( 'wp_enqueue_scripts', 'main_styles' ); // Add Theme Stylesheet
function main_styles() {

	/**
	 *
	 * Minified and Concatenated styles
	 *
	 */
	wp_register_style( 'main_style', get_template_directory_uri() . '/style.min.css', array(), '1.0', 'all' );
	wp_enqueue_style( 'main_style' ); // Enqueue it!

	/*Font Awesome*/
	wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );
	/*Google Font*/
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:100,400,700,900|Montserrat:400,700', false );

}

/**
 *
 * Scripts: Frontend with no conditions, Add Custom Scripts to wp_head
 *
 * @since  1.0.0
 *
 */
add_action( 'wp_enqueue_scripts', 'main_scripts' );
function main_scripts() {
	if ( $GLOBALS['pagenow'] != 'wp-login.php' && ! is_admin() ) {

		wp_enqueue_script( 'jquery' ); // Enqueue it!
	
		wp_register_script( 'main_bootstrapJS', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js#asyncload' ); // Custom scripts
		wp_enqueue_script( 'main_bootstrapJS' ); // Enqueue it!
		
		wp_register_script( 'main_customJS', get_template_directory_uri() . '/assets/js/custom.min.js#asyncload' ); // Custom scripts
		wp_enqueue_script( 'main_customJS' ); // Enqueue it!
	}

}