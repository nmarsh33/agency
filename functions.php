<?php
/**
 * Meditheme functions and definitions
 *
 *
 */

/**
 * Theme setup
 *
 */
require get_template_directory() . '/inc/setup.php';

/**
 * Load functions to secure your WP install.
 *
 */
require get_template_directory() . '/inc/security.php';

/**
 * Enqueue styles and scripts
 *
 */
require get_template_directory() . '/inc/enqueue.php';


/**
 * Custom Post Types
 *
 */
require get_template_directory() . '/inc/custom-posts.php';


/**
 * Theme Settings
 *
 */
require get_template_directory() . '/inc/theme-options.php';











